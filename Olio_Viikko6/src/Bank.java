/*
Arttu Ristola 0418747
28.10.2015
*/

import java.util.ArrayList;
import java.util.Scanner;


    

public class Bank {
    Scanner scan = new Scanner(System.in);
    ArrayList<Account> Tilit;
    
    public Bank(){
    Tilit = new ArrayList<Account>();

    
    }
    void addAccount() {
        System.out.print("Syötä tilinumero: ");
        String acc = scan.next();
        System.out.print("Syötä rahamäärä: ");
        int raha = scan.nextInt();
        
        
        Tilit.add(new Debit(acc, raha, 0));
        System.out.println("Tili luotu.");
        
    }
    void addCredit() {
        System.out.print("Syötä tilinumero: ");
        String acc = scan.next();
        System.out.print("Syötä rahamäärä: ");
        int raha = scan.nextInt();
        System.out.print("Syötä luottoraja: ");
        int raja = scan.nextInt();
        
        Tilit.add(new Credit(acc, raha, raja));
        System.out.println("Tili luotu.");
        
    }
    
    
    void eraseAccount() {
        System.out.print("Syötä poistettava tilinumero: ");
        String acc = scan.next();
        
        int index = tilinhaku(acc);
        if (index < 0) System.out.println("Haettua tiliä ei löydy.");
        else {
            Tilit.remove(index);
            System.out.println("Tili poistettu.");
        }
    }

    void getAccount() {
        System.out.print("Syötä tulostettava tilinumero: ");
        String acc = scan.next();
        int index = tilinhaku(acc);
        if (index < 0) System.out.println("Haettua tiliä ei löydy.");
        else {
            System.out.print("Tilinumero: " + Tilit.get(index).tili);
            System.out.print(" Tilillä rahaa: " + Tilit.get(index).saldo);
            if (Tilit.get(index).luottoraja != 0) {
                System.out.print(" Luottoraja: " + Tilit.get(index).luottoraja);
            }
        System.out.println();
        }
    }
    
    int tilinhaku(String data){
        int index = -1;
        int i;
        
        for (i = 0; i < Tilit.size(); i++) {
            if (Tilit.get(i).tili.equals(data)) {
                index = i;
                break;
            }           
        }              
        return index;
    }
    
    void getAll() {
        int i = 0;
        System.out.println("Kaikki tilit:");
        for (i = 0; i < Tilit.size(); i++) {
            System.out.print("Tilinumero: " + Tilit.get(i).tili);
            System.out.print(" Tilillä rahaa: " + Tilit.get(i).saldo);
            if (Tilit.get(i).luottoraja != 0) {
                System.out.print(" Luottoraja: " + Tilit.get(i).luottoraja);
            }
        System.out.println();
        }
    }
        
    void saveMoney() {
        System.out.print("Syötä tilinumero: ");
        String acc = scan.next();
        System.out.print("Syötä rahamäärä: ");
        int raha = scan.nextInt();
        int index = tilinhaku(acc);
        if (index < 0) System.out.println("Haettua tiliä ei löydy.");
        else {
            Tilit.get(index).saldo = Tilit.get(index).saldo + raha;
            //System.out.println("Talletetaan tilille: " + acc + " rahaa " + raha);
        }    
    }
    
    void draw() {
        System.out.print("Syötä tilinumero: ");
        String acc = scan.next();
        System.out.print("Syötä rahamäärä: ");
        int raha = scan.nextInt();
        
        int index = tilinhaku(acc);
        if (index < 0) System.out.println("Haettua tiliä ei löydy.");
        else {
            if (Tilit.get(index).saldo >= raha){
                Tilit.get(index).saldo = Tilit.get(index).saldo - raha;
            }
            else if (Tilit.get(index).saldo + Tilit.get(index).luottoraja >= raha){
                Tilit.get(index).saldo = Tilit.get(index).saldo - raha;
            }
            else {
                System.out.println("Tilillä ei ole tarpeeksi rahaa.");
            }
            //System.out.println("Nostetaan tililtä: " + acc + " rahaa " + raha);
        }
    }
}


/* eof */