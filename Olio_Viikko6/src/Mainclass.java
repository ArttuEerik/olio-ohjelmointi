/*
Arttu Ristola 0418747
28.10.2015
*/

import java.util.Scanner;


public class Mainclass {


    
    public static void main(String[] args) {
        int choise;
        boolean totuus = true;
        //Tili account = new Tili("000-000", 0);   
        
        Scanner scan = new Scanner(System.in);
        Bank pankki = new Bank();
    
    
        while (totuus) {
        
        System.out.println();
        
            System.out.println("*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            choise = scan.nextInt();
            

            switch(choise){
                case 1:
                    pankki.addAccount();
                    break;
                case 2:
                    pankki.addCredit();
                    break;
                case 3:
                    pankki.saveMoney();
                    break;
                case 4:
                    pankki.draw();
                    break;
                case 5:
                    pankki.eraseAccount();
                    break;
                case 6:
                    pankki.getAccount();
                    break;
                case 7:
                    pankki.getAll();
                    break;
                case 0:
                    totuus = false;
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
                
            }
        }
    }
}


/* eof */