package Koira;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Arttu
 */
public class Mainclass {
  
    
    public static void main(String[] args) throws IOException {
                     
        String nimi;
        String lause;

        Scanner scan = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("Anna koiralle nimi: ");
        nimi = scan.next();
        Dog d1 = new Dog(nimi);
        
        
        System.out.println("Mitä koira sanoo: ");
        lause = br.readLine();
                
        d1.speak(lause);
        
    }
    
}
