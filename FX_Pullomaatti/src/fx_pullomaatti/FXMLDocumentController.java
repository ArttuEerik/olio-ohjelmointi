/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx_pullomaatti;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import oldClasses.Bottle;
import oldClasses.BottleDispenser;




public class FXMLDocumentController implements Initializable {
    
    
    BottleDispenser bd = BottleDispenser.getInsatance();
    int value;
    String text;
    
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Label textField;
    @FXML
    private Slider moneySlider;   
    @FXML
    private ComboBox<Bottle> laatikko;
    @FXML
    private Button ticketButton;
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        laatikko.getItems().addAll(bd.getlist());

        
    }    

    @FXML
    private void moneyIn(ActionEvent event) {
        
        textField.setText("");
        textField.setText(bd.addMoney(moneySlider.getValue()));
        moneySlider.setValue(0);
       
    }

    @FXML
    private void buyButton(ActionEvent event) {
        textField.setText("");
        if(laatikko.getValue() == null){
            textField.setText("Ei pulloa valittu!");
        }
        else{
            textField.setText(bd.tryBuy(laatikko.getValue()));
            text = "***KUITTI***\nOstettu tuote: " + laatikko.getValue().getName() + " " + laatikko.getValue().getSize() + "\nHinta: " + laatikko.getValue().getPrice();
            laatikko.getItems().clear();
            laatikko.getItems().addAll(bd.getlist());
        }
        
    }

    @FXML
    private void moneyOut(ActionEvent event) {
        textField.setText(bd.returnMoney());
        
    }

    @FXML
    private void ticketPrint(ActionEvent event) {
    
        String file = "Kuitti.txt";
               
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            out.write(text);
            out.close();            
        }
        catch (IOException ex) {
            System.out.println(System.getProperty("user.dir"));
        }        
        textField.setText("Kuitti tulostettu.");
    }
    @FXML
    private void getBottle(ActionEvent event) {
        
    }
}
