package oldClasses;


import java.util.ArrayList;
import fx_pullomaatti.FXMLDocumentController;



public class BottleDispenser {
    
    //private int bottles;
    private double money;
    private double input; /*Kertoo laitteeseen syötettyjen, käyttämättömien rahojen määrän*/
    
    private ArrayList<Bottle> list = new ArrayList<Bottle>();
    
    static private BottleDispenser laite = null; /*Luodaan vain yksi laite singletonin mukaisesti */

   
    
    private BottleDispenser() {

        money = 0;
        input = 0;
    
        list.add(new Bottle());
        list.add(new Bottle("Pepsi Max", 1.5, 2.2));
        list.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        list.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        list.add(new Bottle("Fanta Zero", 0.5, 1.95));
        list.add(new Bottle("Fanta Zero", 0.5, 1.95));
        
    }
    
    static public BottleDispenser getInsatance() { /* Voidaan käyttää vain yhtä oliota, joten aina palautetaan sama laite */
        if (laite == null) {
            laite = new BottleDispenser();
        }
                
        return laite;
    }
   public ArrayList<Bottle> getlist() {
       return list;
   }
   
    public String addMoney(double raha) {
        money += 1;
        input += raha;
        String maara = "Lisäsit rahaa " + raha + "€";
        return(maara);
        
    }
    
    private String buyBottle(Bottle b) {

        input = input - b.getPrice();
        String s = "KACHUNK! " + b.getName() + " " + b.getSize() + " tipahti masiinasta!";
        //list.set(m, null);
        list.remove(b);
        return s;
        
    }
    
    public String returnMoney() {
        money = 0;
        
        String p = ("Klink klink. Rahaa tuli ulos " + input*100/100 + "€\n");
        input = 0;
        return p;   
    }
    
    private String noMoney() {
        return ("Syötä rahaa ensin!");
    }
    
    public String tryBuy(Bottle b) {     
        if (input >= b.getPrice()) 
            return buyBottle(b);
        
        else 
            return noMoney();
    }
    
    public void listBottles() {
        //System.out.println("Pulloja koneessa: " + bottles);
        for (int j = 0; j < list.size(); j++) {
            System.out.print(j+1 + ". ");
            System.out.print(list.get(j));
            System.out.println();
            
        }
        
    }
    public int getlistsize() {
        return list.size();
    }
    
}
