package oldClasses;


/**
 *
 * @author Arttu
 */
public class Bottle {
    
    private String name;
    private String manuf;
    private double energy;
    private double size;
    private double price;
    
    public Bottle() {
        name = "Pepsi Max";
        manuf = "Pepsi";
        energy = 0.3;
        size = 0.5;
        price = 1.80;
        
        
    }
    public Bottle(String nimi, double koko, double hinta) {
        name = nimi;
        manuf = "Pepsi";
        energy = 0.3;
        size = koko;
        price = hinta;
               
    }
   
    
    @Override
    public String toString() {
        return ("Nimi: " + name + " Koko: " + size + " Hinta: " + price); 
    }

    public String getName() {
        return name;
    }

    public String getManuf() {
        return manuf;
    }

    public double getEnergy() {
        return energy;
    }

    public double getSize() {
        return size;
    }

    public double getPrice() {
        return price;
    }
}
