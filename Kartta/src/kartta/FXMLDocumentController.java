/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta;

import java.awt.Color;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;




public class FXMLDocumentController implements Initializable {
    @FXML
    private AnchorPane background;
    
    Circle c;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void drawDot(MouseEvent event) {
        c = new Circle(event.getSceneX(), event.getSceneY(), 5);
        background.getChildren().add(c);
       
    }
    
}
