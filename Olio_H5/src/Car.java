/*
Arttu Ristola 14.10.2015
*/
import java.util.ArrayList;


class Body{
    public Body() {
        System.out.println("Valmistetaan: Body");
    }
}
class Chassis{
    public Chassis() {
        System.out.println("Valmistetaan: Chassis");
    }
}
class Engine{
    public Engine() {
        System.out.println("Valmistetaan: Engine");
    }
}
class Wheel{
    public Wheel() {
        System.out.println("Valmistetaan: Wheel");
       
    }
}

public class Car {
    private int i;
    private Body runko;
    private Chassis alusta;
    private Engine moottori;

    ArrayList<Wheel> renkaat;
    
            
    public Car() {
        renkaat = new ArrayList<Wheel>();
        runko = new Body();
        alusta = new Chassis();
        moottori = new Engine();
        for(i = 0; i < 4; i++){
            renkaat.add(new Wheel());
        }
    }
    
    public void print(){
       System.out.println("Autoon kuuluu:");
       System.out.printf("\t%s\n", runko.getClass().getName());
       System.out.printf("\t%s\n", alusta.getClass().getName());
       System.out.printf("\t%s\n", moottori.getClass().getName());
       System.out.printf("\t%d %s\n", renkaat.size(), renkaat.get(0).getClass().getName());
       
    }
}
