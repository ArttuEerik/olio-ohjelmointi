/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teatteriohjelma;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import teatteriohjelma.Theatre;
import teatteriohjelma.LIST;




public class FXMLDocumentController implements Initializable {
    
    LIST L;
  
    
    private Label label;
    @FXML
    private ComboBox<Theatre> setTheatre;
    @FXML
    private TextField showday;
    @FXML
    private TextField timeEnd;
    @FXML
    private TextField timeStart;
    @FXML
    private Button listButton;
    @FXML
    private TextField movieName;
    @FXML
    private Button searchButton;
    @FXML
    private ListView<movie> movieList;

    public FXMLDocumentController() throws IOException {
        this.L = new LIST();
        
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb){
             
        setTheatre.getItems().addAll(L.getList());
        
        
    }    


    @FXML
    private void listMovies(ActionEvent event) throws MalformedURLException, IOException {
        String id = setTheatre.getValue().getID();
        String date = showday.getText();
        String u = "http://www.finnkino.fi/xml/Schedule/?area="+id+"&dt="+date;
        URL url = new URL(u);
        Movies M = new Movies(url);
        ArrayList<movie> temp = new ArrayList<>();
        movieList.setItems(null);
        if(timeStart.getText().isEmpty() && timeEnd.getText().isEmpty()) {
            temp = M.getList();
        }
            
        else {
            //Tähän tulostukset elokuville, jotka ovat aikahaarukan sisällä
            for(int i = 0; i < M.getList().size(); i++) {
                try {
                    SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
                    Date s = parser.parse(M.getList().get(i).getStart());
                    
                    Date time1 = parser.parse(timeStart.getText());
                    Date time2 = parser.parse(timeEnd.getText());
                    
                    if (s.after(time1)) {
                        if (s.before(time2)) {
                            
                            temp.add(M.getList().get(i));
                        }
                    }
                } catch (ParseException e) {
                    // Invalid date was entered
                }
            }
        }
        
        movie buff[]=new movie[temp.size()];       
        temp.toArray(buff);
        ObservableList<movie> data = FXCollections.observableArrayList(buff);
        movieList.setItems(data);
        
  
    }

    @FXML
    private void searchMovie(ActionEvent event) {
        
    }
}
