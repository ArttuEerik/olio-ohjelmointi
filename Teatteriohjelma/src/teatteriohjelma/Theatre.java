/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teatteriohjelma;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class Theatre {
    
      
      
      private String name;
      private String id;
    
    public Theatre(String n, String i) {
        this.id = i;
        this.name = n;
    
    }
    
    @Override
        public String toString() {
            return name;
        }

        public String getID() {
            return id;
        }

       public String getName() {
           return name;
       }
            
}
 
class LIST {

    
    private String content;
    private Document doc;
    private ArrayList<Theatre> list;
    
    LIST() throws MalformedURLException, IOException {
       
        URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        content = "";
        String line;
        
        while((line = br.readLine()) != null) {
            content += line + "\n";
        
        }
        br.close();
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();        
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            //doc.getDocumentElement().normalize();
            list = new ArrayList<>();
            
            NodeList nodes = doc.getElementsByTagName("TheatreArea");
            for(int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE) {
                   Element e = (Element) node;
                    String id = e.getElementsByTagName("ID").item(0).getTextContent();
                    String name = e.getElementsByTagName("Name").item(0).getTextContent();
                    list.add(new Theatre(name, id));
                }
            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Theatre.class.getName()).log(Level.SEVERE, null, ex);
        }
    list.remove(0);
    }  

    /**
     * @return the list
     */
    public ArrayList<Theatre> getList() {
        return list;
    }
}

class movie {
    String id;
    String name;
    private String start;
    private String end;
    
    movie(String n, String i, String s, String e) {
        this.id = i;
        this.name = n;
        
        s = s.substring(11, s.length() - 3);
        e = e.substring(11, e.length() - 3);
        
        this.start = s;
        this.end = e;
                
    }
    @Override
        public String toString() {
            return name + " " + start + "-" + end;
        }

    /**
     * @return the start
     */
    public String getStart() {
        return start;
    }

    /**
     * @return the end
     */
    public String getEnd() {
        return end;
    }
}

class Movies {
    private String content;
    private Document doc;
    private ArrayList<movie> list;
    
    Movies(URL url) throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        content = "";
        String line;
        
        while((line = br.readLine()) != null) {
            content += line + "\n";
        
        }
        br.close();
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();        
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(content)));            
            list = new ArrayList<>();
            
            NodeList nodes = doc.getElementsByTagName("Show");
            for(int i = 0; i < nodes.getLength(); i++) {                
                Node node = nodes.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE) {
                   Element e = (Element) node;
                    String id = e.getElementsByTagName("ID").item(0).getTextContent();
                    String name = e.getElementsByTagName("Title").item(0).getTextContent();
                    String start = e.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                    String end = e.getElementsByTagName("dttmShowEnd").item(0).getTextContent();
                    list.add(new movie(name, id, start, end));
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Theatre.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the list
     */
    public ArrayList<movie> getList() {
        return list;
    }
    public void remo(int i) {
        list.remove(i);
    }
}