
package browser;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

public class FXMLDocumentController implements Initializable {
    
    String adress;
    ArrayList<String> prev;
    ArrayList<String> nxt;
    
    private Label label;
    @FXML
    private TextField adressBar;
    @FXML
    private Button webButton;
    @FXML
    private WebView webPage;
    @FXML
    private Button refresh;
    @FXML
    private Button InitButton;
    @FXML
    private Button orderButton;
    @FXML
    private Button preButton;
    @FXML
    private Button nextButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        adress = "http://www.lut.fi";
        prev = new ArrayList<>();
        nxt = new ArrayList<>();
        webPage.getEngine().load(adress);        
    }    

    @FXML
    private void goToWeb(ActionEvent event) throws MalformedURLException {
        //tallennetaan vieraillun sivuston osoite listaan        
        prev.add(0, adress);
        if(prev.size() > 9)
            prev.remove(prev.size());
        
        //jos halutaan ladata index.html 
        if(adressBar.getText().equals("index.html")) {
            adress = getClass().getResource("index.html").toString();
        }
        //Jos halutaan ladata nettisivu
        else { 
            adress = adressBar.getText();
            if(!adress.contains("http://")) {
                adress = "http://" + adress;
            }
        }
        webPage.getEngine().load(adress);
               
    }

    @FXML
    private void freshPage(ActionEvent event) {
        webPage.getEngine().load(adress);
        adressBar.setText(adress);
    }

    @FXML
    private void order(ActionEvent event) {
        //webPage.getEngine().load(getClass().getResource("index.html").toString());
        webPage.getEngine().executeScript("document.shoutOut()");
    }
    
    @FXML
    private void toBack(ActionEvent event) {
        webPage.getEngine().executeScript("initialize()");
    }

    @FXML
    private void prePage(ActionEvent event) {
        nxt.add(0, adress);
        if(nxt.size() > 9)
            nxt.remove(nxt.size());
        adress = prev.get(0);
        webPage.getEngine().load(adress);
        prev.remove(0);
        adressBar.setText(adress);
    }

    @FXML
    private void nextPage(ActionEvent event) {
        prev.add(0, adress);
        if(nxt.size() > 9)
            nxt.remove(nxt.size());
        adress = nxt.get(0);
        webPage.getEngine().load(adress);
        nxt.remove(0);
        adressBar.setText(adress);
    } 
}
