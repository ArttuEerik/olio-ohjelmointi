package Koira;
/**
 *
 * @author Arttu
 */
public class Dog { //Luokan määritys
    private int totuus = 0;
    private String nimi = "Doge";
    private String lause = "Much wow!";
    private String[] sanat;

    Dog(String x) { //Rakentaja
        if (x.isEmpty()) nimi = "Doge";
        else nimi = x;
        System.out.println("Hei, nimeni on " + nimi);
        
    }
    
    
    public int speak(String y) { //Puhumismetodi
        
        if (y.isEmpty()) {//Jos syöte on tyhjä
            
            System.out.println(nimi + ": " + lause);
        }
        else {
            lause = y;
            totuus = 1;
            //Jaetaan scannattu lause taulukkoon
            sanat = lause.split(" ");
            
            for (int i = 0; sanat.length > i; i++ ){//Käydään taulukko läpi
                //Tarkastetaan Booleanit
                if (sanat[i].equalsIgnoreCase("true") || sanat[i].equalsIgnoreCase("false")) {
                    System.out.println("Such boolean: " + sanat[i]);
                }
                //Tarkastetaan numerot
                else if (sanat[i].matches("^-?\\d+$")) {
                    System.out.println("Such integer: " + sanat[i]);
                }
                //Muut 
                else {
                    System.out.println(sanat[i]);
                }
            }
        
        }
        
        return(totuus);
        
    }
    
    
}