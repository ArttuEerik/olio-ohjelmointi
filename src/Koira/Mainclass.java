package Koira;

import java.util.Scanner;


/**
 *
 * @author Arttu
 */
public class Mainclass {
  
    
    public static void main(String[] args) {
                     
        String nimi;
        String lause;
        int totuus = 0;
        
        Scanner scan = new Scanner(System.in);
        //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.print("Anna koiralle nimi: ");
        nimi = scan.nextLine();
        Dog d1 = new Dog(nimi);
        
        
        while(totuus ==0) {
            System.out.print("Mitä koira sanoo: ");
            lause = scan.nextLine();
            totuus = d1.speak(lause.trim());
        }
        
        
    }
    
}
