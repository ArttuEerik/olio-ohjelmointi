/*
 * Arttu Ristola op. 0418747
 * 7. Lokakuuta 2015
 */

import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.io.*;

public class ReadAndWriteIO {
    
    private String filename;
    
    public ReadAndWriteIO(){       
        
    }
    public void readzip(String f) throws IOException {
        
        filename = f;
        final int BUFFER = 2048;
        
        BufferedOutputStream dest = null;
        FileInputStream fis = new FileInputStream(filename);
        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
        ZipEntry entry;
        
        while ((entry = zis.getNextEntry()) != null) {
            int count;
            byte data[] = new byte[BUFFER];
            
            FileOutputStream fos = new FileOutputStream(entry.getName());
            
            count = zis.read(data, 0, BUFFER);
            String teksti = new String(data);
            System.out.println(teksti);                    
        }
        zis.close();    
        
    }
    
    public void readFile(String f) throws FileNotFoundException, IOException {
        filename = f;
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            String s = in.readLine();
            while (s != null) {
                System.out.println(s);
                s = in.readLine();
            }
        }
        catch (IOException ex) {
            System.out.println( System.getProperty( "user.dir" ) );
        }   
    }
    
    public void ReadAndWrite(String r, String w) {
        String readfile = r;
        String writefile = w;
        
        try {
            BufferedReader in = new BufferedReader(new FileReader(readfile));
            BufferedWriter out = new BufferedWriter(new FileWriter(writefile));
            String s = in.readLine();
            while (s != null) {
                if (s.length() < 30 && s.length() > 0 && s.trim().length() != 0){
                    if (s.contains("v")) {
                        out.write(s);
                        out.newLine();
                    }
                }
                s = in.readLine();
            }
            in.close();
            out.close();
        }
        catch (IOException ex) {
            System.out.println( System.getProperty( "user.dir" ));
        } 
    }
}


/* Esimerkkimetodeja:
    public void writeText(String text) throws FileNotFoundException, IOException{
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        out.write(text);
        out.close();
    }
    
    public String readText() throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(filename));
        String s = in.readLine();
        in.close();
        return in.readLine();
    }
    */