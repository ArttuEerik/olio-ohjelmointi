/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teatteri;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


/**
 *
 * @author Arttu
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private ComboBox<?> setTheatre;
    @FXML
    private TextField showday;
    @FXML
    private TextField timeEnd;
    @FXML
    private TextField timeStart;
    @FXML
    private Button listButton;
    @FXML
    private TextField movieName;
    @FXML
    private Button searchButton;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void setDay(ActionEvent event) {
    }

    @FXML
    private void setTimeEnd(ActionEvent event) {
    }

    @FXML
    private void setTimeStart(ActionEvent event) {
    }

    @FXML
    private void listMovies(ActionEvent event) {
    }

    @FXML
    private void setMovieName(ActionEvent event) {
    }

    @FXML
    private void searchMovie(ActionEvent event) {
    }
    
}
