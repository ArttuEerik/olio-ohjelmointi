
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Arttu
 */
public class Mainclass {

    
    
    
    public static void main(String[] args) {
        boolean totuus = false;
        int valinta;
        int valinta2;
        Scanner scan = new Scanner(System.in);
        //Bottle pullo = new Bottle();
        BottleDispenser bd = BottleDispenser.getInsatance();
        
        
        while(totuus == false) {
            System.out.println();
            System.out.println("*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            
            System.out.print("Valintasi: ");
            valinta = scan.nextInt();
            
            switch(valinta) {
                case 1:
                    bd.addMoney();
                    break;
                case 2:
                    bd.listBottles();
                    System.out.print("Valintasi: ");
                    valinta2 = scan.nextInt();  
                    valinta2--;
                    bd.tryBuy(valinta2);
                    break;
                case 3:
                    bd.returnMoney();
                    break;
                case 4:
                    bd.listBottles();
                    break;
                case 0:
                    totuus = true;
                    break;
                default:
                    System.out.println("Choise not available");
                
            }
        }
        
    }
    
}
