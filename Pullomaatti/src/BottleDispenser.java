
import java.util.ArrayList;



public class BottleDispenser {
    
    //private int bottles;
    private double money;
    private double input; /*Kertoo laitteeseen syötettyjen, käyttämättömien rahojen määrän*/
    
    ArrayList<Bottle> list = new ArrayList<Bottle>();
    
    static private BottleDispenser laite = null;

   
    
    private BottleDispenser() {

        money = 0;
        input = 0;
    
        list.add(new Bottle());
        list.add(new Bottle("Pepsi Max", 1.5, 2.2));
        list.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        list.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        list.add(new Bottle("Fanta Zero", 0.5, 1.95));
        list.add(new Bottle("Fanta Zero", 0.5, 1.95));
        
    }
    
    static public BottleDispenser getInsatance() {
        if (laite == null) {
            laite = new BottleDispenser();
        }
                
        return laite;
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
        input += 1;
    }
    
    private void buyBottle(int m) {
        if (list.size() <= 0) System.out.println("Ei pulloja jäljellä!");
        else {
            
            input = input - list.get(m).getPrice();
            System.out.println("KACHUNK! " + list.get(m).getName() + " tipahti masiinasta!");
            list.remove(m);
        }
    }
    
    public void returnMoney() {
        money = 0;
        
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + "%.2f€\n", input);
        input = 0;
    }
    
    private void noMoney() {
        System.out.println("Syötä rahaa ensin!");
    }
    public void tryBuy(int n) {
        
        if (input >= list.get(n).getPrice()) buyBottle(n);
        else noMoney();
    }
    
    public void listBottles() {
        //System.out.println("Pulloja koneessa: " + bottles);
        for (int j = 0; j < list.size(); j++) {
            System.out.print(j+1 + ". ");
            System.out.print(list.get(j));
            System.out.println();
            
        }
        
    }
    
}
