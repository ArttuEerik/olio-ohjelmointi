import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Label textBox;
    @FXML
    private TextField inputField;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void typeText(KeyEvent event) {
    
        if (event.getCode() == KeyCode.ENTER) {
            textBox.setText(textBox.getText() + "\n" + inputField.getText());
            inputField.clear();
        }
    
        
    }

}
