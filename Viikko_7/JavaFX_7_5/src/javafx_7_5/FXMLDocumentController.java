/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx_7_5;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Arttu
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextArea txtField;
    @FXML
    private Button saveB;
    @FXML
    private Button loadB;
    @FXML
    private TextField inputField;
    @FXML
    private TextField fileName;
    /*
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void saveFile(ActionEvent event) {
        String file = fileName.getText();
        fileName.clear();
        String text = txtField.getText();
        
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            out.write(text);
            out.close();
            
        }
        catch (IOException ex) {
            System.out.println( System.getProperty( "user.dir" ) );
        }
        
        txtField.setText("");
        
        
    }

    @FXML
    private void loadFile(ActionEvent event) {
        String file = fileName.getText();
        String tiedosto = null;
        fileName.clear();
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String s = in.readLine();
            while (s != null) {
                tiedosto = tiedosto + "\n" + s;
                s = in.readLine();
            }
            in.close();
        }
        catch (IOException ex) {
            System.out.println( System.getProperty( "user.dir" ) );
        }
        txtField.setText(tiedosto);
        
        
    }

    @FXML
    private void getText(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            txtField.setText(txtField.getText() + "\n" + inputField.getText());
            inputField.clear();
        }
    }
    
    
}
